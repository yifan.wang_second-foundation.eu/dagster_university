from pathlib import Path

from dagster import RunRequest, SensorResult, sensor, SensorEvaluationContext

import json

from ..jobs import adhoc_request_job


@sensor(
    job=adhoc_request_job
)
def adhoc_request_sensor(context: SensorEvaluationContext) -> SensorResult:
    path_requests = Path("data/requests")

    previous_state = json.loads(context.cursor) if context.cursor else {}

    json_paths = [p for p in path_requests.glob("*.json") if p.is_file()]
    current_state = {p.name: p.stat().st_mtime for p in json_paths}

    # if the file is new or has been modified since the last run, add it to the request queue
    run_requests = [
        _run_request(fpath, fname, m) 
        for fpath, (fname, m) in zip(json_paths, current_state.items()) 
        if previous_state.get(fname) != m]

    cursor = json.dumps(current_state)
    return SensorResult(run_requests=run_requests, cursor=cursor)


def _run_request(fpath: Path, fname: str, last_modified: float) -> RunRequest:
    with open(fpath, "rt") as f:
        request_config = json.load(f)
    adhoc_request = {"config": {"filename": fname, **request_config}}
    run_config = {"ops": {"adhoc_request": adhoc_request}}
    run_key=f"adhoc_request_{fname}_{last_modified}"
    return RunRequest(run_key=run_key, run_config=run_config)