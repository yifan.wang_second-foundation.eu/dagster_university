from dagster import EnvVar, ConfigurableResource
from dagster_duckdb import DuckDBResource
import requests


class MyConnectionResource(ConfigurableResource):
    def get(self, url: str) -> requests.Response:
        return requests.get(url, headers={"user-agnet": "dagster"})


database_resource = DuckDBResource(database=EnvVar("DUCKDB_DATABASE"))
my_conn_resource = MyConnectionResource()
