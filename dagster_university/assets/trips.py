from pathlib import Path

from dagster import asset, OpExecutionContext
from dagster_duckdb import DuckDBResource
import jinja2 as j2
import yaml

from . import constants, _metadata
from ..resources import MyConnectionResource
from ..partitions import monthly_partition

_PATH_MOD = Path(__file__).parent

_J2ENV = j2.Environment(loader=j2.FileSystemLoader(_PATH_MOD))


@asset(
    partitions_def=monthly_partition,
    group_name="raw_files",
    description="The raw parquet files for the taxi trips dataset. Sourced from the NYC Open Data portal."
)
def taxi_trips_file(context: OpExecutionContext, request: MyConnectionResource) -> None:
    partition_date_str = context.asset_partition_key_for_output()
    month_to_fetch = partition_date_str[:-3]

    url_template = (
        "https://d37ci6vzurychx.cloudfront.net/trip-data/yellow_tripdata_{}.parquet"
    )
    raw_trips = request.get(url_template.format(month_to_fetch))

    path_file = constants.TAXI_TRIPS_TEMPLATE_FILE_PATH.format(month_to_fetch)
    _metadata.write_record_and_log(path_file, raw_trips.content, context)


@asset(
    group_name="raw_files",
)
def taxi_zones_file(context: OpExecutionContext, request: MyConnectionResource) -> None:
    """The raw CSV file for the taxi zones dataset. Sourced from the NYC Open Data portal."""
    url = (
        "https://data.cityofnewyork.us/api/views/755u-8jsi/rows.csv?accessType=DOWNLOAD"
    )
    raw_zones = request.get(url)

    path_file = constants.TAXI_ZONES_FILE_PATH
    _metadata.write_record_and_log(path_file, raw_zones.content, context)


@asset(
    deps=["taxi_trips_file"],
    partitions_def=monthly_partition,
    group_name="ingested",
    description="The raw taxi trips dataset, loaded into a DuckDB database."
)
def taxi_trips(context: OpExecutionContext, database: DuckDBResource) -> None:
    partition_date_str = context.asset_partition_key_for_output()
    month_to_fetch = partition_date_str[:-3]

    path_data = constants.TAXI_TRIPS_TEMPLATE_FILE_PATH.format(month_to_fetch)
    kwargs = dict(path_data=path_data, month_to_fetch=month_to_fetch)
    for key in ("db", "tpl"):
        k = f"{key}_cols"
        with open(_PATH_MOD / "yaml" / f"taxi_trips_{k}.yaml", "rt") as f:
            kwargs[k] = yaml.safe_load(f)
    kwargs["tpl_cols"][f"'{month_to_fetch}'"] = "partition_date"

    sql_query = _J2ENV.get_template("sql/taxi_trips.sql.j2").render(**kwargs)

    with database.get_connection() as conn:
        conn.execute(sql_query)

    context.log.info("Query successful.")


@asset(
    deps=["taxi_zones_file"],
    group_name="ingested",
    description="The raw taxi zones dataset, loaded into a DuckDB database."
)
def taxi_zones(context: OpExecutionContext, database: DuckDBResource) -> None:
    kwargs = {"path_data": constants.TAXI_ZONES_FILE_PATH}
    sql_query = _J2ENV.get_template("sql/taxi_zones.sql.j2").render(**kwargs)

    with database.get_connection() as conn:
        conn.execute(sql_query)

    context.log.info("Query successful.")
