from pathlib import Path

from dagster import asset, OpExecutionContext
from dagster_duckdb import DuckDBResource

import geopandas as gpd
import jinja2 as j2
import yaml
from plotly import express as px

from . import constants, _metadata
from ..partitions import weekly_partition

_PATH_MOD = Path(__file__).parent
_J2ENV = j2.Environment(loader=j2.FileSystemLoader(_PATH_MOD))


@asset(
    deps=["taxi_trips", "taxi_zones"],
    description="Metrics on taxi trips in Manhattan."
)
def manhattan_stats(context: OpExecutionContext, database: DuckDBResource) -> None:
    borough = "Manhattan"
    sql_query = _J2ENV.get_template("sql/stats.sql.j2").render(borough=borough)

    with database.get_connection() as conn:
        trips_by_zone = conn.execute(sql_query).fetch_df()

    trips_by_zone["geometry"] = gpd.GeoSeries.from_wkt(trips_by_zone["geometry"])
    trips_by_zone = gpd.GeoDataFrame(trips_by_zone)

    path_file = constants.MANHATTAN_STATS_FILE_PATH
    _metadata.write_record_and_log(path_file, trips_by_zone.to_json(), context)


@asset(
    deps=["manhattan_stats"],
    description="A map of the number of trips per taxi zone in Manhattan."
)
def manhattan_map(context: OpExecutionContext) -> None:
    trips_by_zone: gpd.GeoDataFrame = gpd.read_file(constants.MANHATTAN_STATS_FILE_PATH)
    context.log.info(f"trips_by_zone loaded with columns {trips_by_zone.columns}")

    fig = px.choropleth_mapbox(
        trips_by_zone,
        geojson=trips_by_zone.geometry.__geo_interface__,
        locations=trips_by_zone.index,
        color="num_trips",
        color_continuous_scale="Plasma",
        mapbox_style="carto-positron",
        center=dict(lat=40.758, lon=-73.985),
        zoom=11,
        opacity=0.7,
        labels={"num_trips": "Number of Trips"},
    )
    context.log.info("fig generated")

    _metadata.write_image_and_log(constants.MANHATTAN_MAP_FILE_PATH, fig, context)


@asset(
    deps=["taxi_trips"],
    partitions_def=weekly_partition,
    description="The number of trips per week, aggregated by week."
)
def trips_by_week(context: OpExecutionContext, database: DuckDBResource) -> None:
    period_to_fetch = context.asset_partition_key_for_output()
    context.log.info(f"{period_to_fetch}, {type(period_to_fetch)}")
    path_data = constants.TRIPS_BY_WEEK_FILE_PATH
    kwargs = dict(period_to_fetch=period_to_fetch, path_data=path_data)
    k = "db_cols"
    with open(_PATH_MOD / "yaml" / f"trips_by_week_{k}.yaml", "rt") as f:
        kwargs[k] = yaml.safe_load(f)

    sql_query = _J2ENV.get_template("sql/trips_by_week.sql.j2").render(**kwargs)

    with database.get_connection() as conn:
        conn.execute(sql_query)

    context.log.info("Query successful.")
