import base64
from io import BytesIO

import pyarrow as pa
from pyarrow import parquet as pqt
import json
from plotly import graph_objs as go, io as pio

from dagster import OpExecutionContext, MetadataValue


def write_record_and_log(fpath: str, content: bytes | str, context: OpExecutionContext) -> None:
    mode = "wb" if isinstance(content, bytes) else "wt"
    with open(fpath, mode) as output_file:
        nbytes = output_file.write(content)

    msg = f"{nbytes} byte" + ("s" if nbytes > 1 else "") + f" written to {fpath}."
    context.log.info(msg)

    ext = fpath.split(".")[-1]
    num_rows = eval(f"_num_rows_{ext}(content)")

    context.add_output_metadata({"Number of records": MetadataValue.int(num_rows)})


def write_image_and_log(fpath: str, fig: go.Figure, context: OpExecutionContext) -> None:
    pio.write_image(fig, fpath)

    with BytesIO() as f:
        pio.write_image(fig, f)
        f.seek(0)
        image_data = f.read()

    context.log.info(f"Data written to {fpath}.")

    base64_data = base64.b64encode(image_data).decode("utf-8")
    md_content = f"![Image](data:image/png;base64,{base64_data})"

    context.add_output_metadata({"preview": MetadataValue.md(md_content)})


def _num_rows_parquet(content: bytes) -> int:
    with pa.BufferReader(content) as f:
        num_rows = pqt.read_table(f).num_rows

    return num_rows


def _num_rows_csv(content: bytes) -> int:
    num_rows = content.decode().count("\n")

    return num_rows


def _num_rows_geojson(content: str) -> int:
    num_rows = len(json.loads(content)["features"])

    return num_rows
