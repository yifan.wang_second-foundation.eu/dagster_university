from pathlib import Path

from dagster import asset, Config, OpExecutionContext
from dagster_duckdb import DuckDBResource
import jinja2 as j2
from plotly import express as px

from . import constants, _metadata


_PATH_MOD = Path(__file__).parent
_J2ENV = j2.Environment(loader=j2.FileSystemLoader(_PATH_MOD))


class AdhocRequestConfig(Config):
    filename: str
    borough: str
    start_date: str
    end_date: str


@asset(
    deps=["taxi_zones", "taxi_trips"],
    description="The response to an request made in the `requests` directory.\nSee `requests/README.md` for more information."
)
def adhoc_request(
    context: OpExecutionContext, config: AdhocRequestConfig, database: DuckDBResource
) -> None:
    file_path = constants.REQUEST_DESTINATION_TEMPLATE_FILE_PATH.format(config.filename.split(".")[0])

    kwargs = {a: getattr(config, a) for a in ("start_date", "end_date", "borough")}
    sql_query = _J2ENV.get_template("sql/adhoc_request.sql.j2").render(**kwargs)
    with database.get_connection() as conn:
        results = conn.execute(sql_query).fetch_df()

    fig = px.bar(
        results,
        x="hour_of_day",
        y="num_trips",
        color="day_of_week",
        barmode="stack",
        title="Number of trips by hour of day in {borough}, from {start_date} to {end_date}".format(**kwargs),
        labels={
            "hour_of_day": "Hour of Day",
            "day_of_week": "Day of Week",
            "num_trips": "Number of Trips"
        }
    )

    _metadata.write_image_and_log(file_path, fig, context)
