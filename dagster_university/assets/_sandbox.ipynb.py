# %% [markdown]
# Tester

from pathlib import Path

import jinja2 as j2
import yaml
import pyarrow as pa
from pyarrow import parquet as pqt

from dagster_university.assets import constants

_PATH_MOD = Path(__file__).parent
_J2ENV = j2.Environment(loader=j2.FileSystemLoader(_PATH_MOD))

# %%
month_to_fetch = "2023-03"
path_data = constants.TAXI_TRIPS_TEMPLATE_FILE_PATH.format(month_to_fetch)
kwargs = dict(path_data=path_data, month_to_fetch=month_to_fetch)
for key in ("db", "tpl"):
    k = f"{key}_cols"
    with open(_PATH_MOD / "yaml" / f"taxi_trips_{k}.yaml", "rt") as f:
        kwargs[k] = yaml.safe_load(f)
kwargs["tpl_cols"][f"'{month_to_fetch}'"] = "partition_date"

sql_query = _J2ENV.get_template("sql/taxi_trips.sql.j2").render(**kwargs)

print(sql_query)

# %%
period_to_fetch = "2023-01-01"
path_data = constants.TRIPS_BY_WEEK_FILE_PATH
kwargs = dict(period_to_fetch=period_to_fetch, path_data=path_data)
k = "db_cols"
with open(_PATH_MOD / "yaml" / f"trips_by_week_{k}.yaml", "rt") as f:
    kwargs[k] = yaml.safe_load(f)

sql_query = _J2ENV.get_template("sql/trips_by_week.sql.j2").render(**kwargs)

print(sql_query)

# %%

with open("data/raw/taxi_trips_2023-01.parquet", "rb") as f:
    content = f.read()

with pa.BufferReader(content) as f:
    table = pqt.read_table(f)

table

# %% [markdown]
# End of Notebook
