from dagster import MonthlyPartitionsDefinition, WeeklyPartitionsDefinition
from ..assets import constants

start_date, end_date = constants.START_DATE, constants.END_DATE

monthly_partition = MonthlyPartitionsDefinition(
    start_date=start_date, end_date=end_date
)

weekly_partition = WeeklyPartitionsDefinition(
    start_date=start_date, end_date=end_date
)
